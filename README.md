# VerneMQ

Deployment of VerneMQ, a high-performance, distributed MQTT message broker

## Client management

Check official docs about this topic here: <https://github.com/vernemq/docker-vernemq#file-based-authentication>

You can set up [File Based Authentication](https://docs.vernemq.com/configuring-vernemq/file-auth) by adding users and passwords as environment variables as follows:

`DOCKER_VERNEMQ_USER_<USERNAME>='password'`

where `<USERNAME> `is the username you want to use. This can be done as many times as necessary to create the users you want. The usernames will always be created in lowercase.

CAVEAT - You cannot have a `=` character in your password.
